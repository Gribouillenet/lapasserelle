<?php add_action( 'init', 'wpm_add_taxonomies_video', 0 );

function wpm_add_taxonomies_video() {

	// Taxonomie Année

	$labels_annee = array(
		'name'              			=> _x( 'Années', 'taxonomy general name'),
		'singular_name'     			=> _x( 'Année', 'taxonomy singular name'),
		'search_items'      			=> __( 'Chercher une année'),
		'all_items'        				=> __( 'Toutes les années'),
		'edit_item'         			=> __( 'Editer l année'),
		'update_item'       			=> __( 'Mettre à jour l année'),
		'add_new_item'     				=> __( 'Ajouter une nouvelle année'),
		'new_item_name'     			=> __( 'Valeur de la nouvelle année'),
		'separate_items_with_commas'	=> __( 'Séparer les réalisateurs avec une virgule'),
		'menu_name'         => __( 'Année'),
	);

	$args_annee = array(

		'hierarchical'      => false,
		'labels'            => $labels_annee,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'annees' ),
	);

	register_taxonomy( 'annees', 'seriestv', $args_annee );

	// Taxonomie Réalisateurs

	$labels_realisateurs = array(
		'name'                       => _x( 'Réalisateurs', 'taxonomy general name'),
		'singular_name'              => _x( 'Réalisateur', 'taxonomy singular name'),
		'search_items'               => __( 'Rechercher un réalisateur'),
		'popular_items'              => __( 'Réalisateurs populaires'),
		'all_items'                  => __( 'Tous les réalisateurs'),
		'edit_item'                  => __( 'Editer un réalisateur'),
		'update_item'                => __( 'Mettre à jour un réalisateur'),
		'add_new_item'               => __( 'Ajouter un nouveau réalisateur'),
		'new_item_name'              => __( 'Nom du nouveau réalisateur'),
		'separate_items_with_commas' => __( 'Séparer les réalisateurs avec une virgule'),
		'add_or_remove_items'        => __( 'Ajouter ou supprimer un réalisateur'),
		'choose_from_most_used'      => __( 'Choisir parmi les plus utilisés'),
		'not_found'                  => __( 'Pas de réalisateurs trouvés'),
		'menu_name'                  => __( 'Réalisateurs'),
	);

	$args_realisateurs = array(
		'hierarchical'          => false,
		'labels'                => $labels_realisateurs,
		'show_ui'               => true,
		'show_admin_column'     => true,
		'update_count_callback' => '_update_post_term_count',
		'query_var'             => true,
		'rewrite'               => array( 'slug' => 'realisateurs' ),
	);

	register_taxonomy( 'realisateurs', 'seriestv', $args_realisateurs );

	// Catégorie de série

	$labels_cat_serie = array(
		'name'                       => _x( 'Catégories de série', 'taxonomy general name'),
		'singular_name'              => _x( 'Catégories de série', 'taxonomy singular name'),
		'search_items'               => __( 'Rechercher une catégorie'),
		'popular_items'              => __( 'Catégories populaires'),
		'all_items'                  => __( 'Toutes les catégories'),
		'edit_item'                  => __( 'Editer une catégorie'),
		'update_item'                => __( 'Mettre à jour une catégorie'),
		'add_new_item'               => __( 'Ajouter une nouvelle catégorie'),
		'new_item_name'              => __( 'Nom de la nouvelle catégorie'),
		'add_or_remove_items'        => __( 'Ajouter ou supprimer une catégorie'),
		'choose_from_most_used'      => __( 'Choisir parmi les catégories les plus utilisées'),
		'not_found'                  => __( 'Pas de catégories trouvées'),
		'menu_name'                  => __( 'Catégories de série'),
	);

	$args_cat_serie = array(

		'hierarchical'          => true,
		'labels'                => $labels_cat_serie,
		'show_ui'               => true,
		'show_admin_column'     => true,
		'query_var'             => true,
		'rewrite'               => array( 'slug' => 'categories-series' ),
	);

	register_taxonomy( 'categoriesseries', 'seriestv', $args_cat_serie );
}

add_action( 'init', 'wpm_add_taxonomies_portfolio', 0 );

//On crée 3 taxonomies personnalisées: Année, Réalisateurs et Catégories de projet.

function wpm_add_taxonomies_portfolio() {

	// Taxonomie Année

	// Déclaration des différentes dénominations de taxonomie qui seront affichées et utilisées dans l'administration de WordPress
	$labels_languages = array(
		'name'              			=> _x( 'Languages', 'taxonomy general name'),
		'singular_name'     			=> _x( 'Language', 'taxonomy singular name'),
		'search_items'      			=> __( 'Chercher un language'),
		'all_items'        				=> __( 'Toutes les languages'),
		'edit_item'         			=> __( 'Editer le language'),
		'update_item'       			=> __( 'Mettre à jour le language'),
		'add_new_item'     				=> __( 'Ajouter un nouveau language'),
		'new_item_name'     			=> __( 'Valeur du nouveau language'),
		'separate_items_with_commas'	=> __( 'Séparer les languages avec une virgule'),
		'menu_name'         => __( 'Language'),
	);

	$args_languages = array(
	// Si 'hierarchical' est défini à false, la taxonomie se comportera comme une étiquette standard
		'hierarchical'      => false,
		'labels'            => $labels_languages,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'languages' ),
	);

	register_taxonomy( 'languages', 'portfolio', $args_languages );

	// Taxonomie Réalisateurs

	$labels_supports = array(
		'name'                       => _x( 'Supports', 'taxonomy general name'),
		'singular_name'              => _x( 'Support', 'taxonomy singular name'),
		'search_items'               => __( 'Rechercher un support'),
		'popular_items'              => __( 'Supports populaires'),
		'all_items'                  => __( 'Tous les supports'),
		'edit_item'                  => __( 'Editer un support'),
		'update_item'                => __( 'Mettre à jour un support'),
		'add_new_item'               => __( 'Ajouter un nouveau support'),
		'new_item_name'              => __( 'Nom du nouveau support'),
		'separate_items_with_commas' => __( 'Séparer les supports avec une virgule'),
		'add_or_remove_items'        => __( 'Ajouter ou supprimer un support'),
		'choose_from_most_used'      => __( 'Choisir parmi les plus utilisés'),
		'not_found'                  => __( 'Pas de supports trouvés'),
		'menu_name'                  => __( 'Supports'),
	);

	$args_supports = array(
		'hierarchical'          => false,
		'labels'                => $labels_supports,
		'show_ui'               => true,
		'show_admin_column'     => true,
		'update_count_callback' => '_update_post_term_count',
		'query_var'             => true,
		'rewrite'               => array( 'slug' => 'supports' ),
	);

	register_taxonomy( 'supports', 'portfolio', $args_supports );

	// Catégorie de projet

	$labels_cat_projet = array(
		'name'                       => _x( 'Catégories de projet', 'taxonomy general name'),
		'singular_name'              => _x( 'Catégories de projet', 'taxonomy singular name'),
		'search_items'               => __( 'Rechercher une catégorie'),
		'popular_items'              => __( 'Catégories populaires'),
		'all_items'                  => __( 'Toutes les catégories'),
		'edit_item'                  => __( 'Editer une catégorie'),
		'update_item'                => __( 'Mettre à jour une catégorie'),
		'add_new_item'               => __( 'Ajouter une nouvelle catégorie'),
		'new_item_name'              => __( 'Nom de la nouvelle catégorie'),
		'add_or_remove_items'        => __( 'Ajouter ou supprimer une catégorie'),
		'choose_from_most_used'      => __( 'Choisir parmi les catégories les plus utilisées'),
		'not_found'                  => __( 'Pas de catégories trouvées'),
		'menu_name'                  => __( 'Catégories de projet'),
	);

	$args_cat_projet = array(
	// Si 'hierarchical' est défini à true, la taxonomie se comportera comme une catégorie standard
		'hierarchical'          => true,
		'labels'                => $labels_cat_projet,
		'show_ui'               => true,
		'show_admin_column'     => true,
		'query_var'             => true,
		'rewrite'               => array( 'slug' => 'categories-projets' ),
	);

	register_taxonomy( 'categoriesprojets', 'portfolio', $args_cat_projet );
}