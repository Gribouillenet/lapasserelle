<?php
/*
Template Name: bibliographie
*/

get_header();
?>

<div id="section-primary" class="section-primary clearfix">

	<?php if(have_posts()) : while(have_posts()) : the_post();

	$url_fond = get_stylesheet_directory_uri() . '/img/svg/top-blc.svg';

	$title_page = get_the_title();
	?>

	<h1 class="container-fluid corner-round corner-round-bibliographie title-page col-12"><?= $title_page ?></h1>

</div>

<div id="section-secondary" class="section-secondary container clearfix">
	<h2>Rapport d'activités</h2>
	<?php
    $posts = get_field('rapports_activites');

    if( $posts ): ?>
		<ul class="row">
	        <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
				<?php
					setup_postdata($post);
					$file = get_field('url');
				?>
				<li class="link-pdf col-12 col-lg-4">
					<a class="d-flex align-items-center" href="<?= $file['url'] ?>" title="Télécharger le fichier : <?= $post->post_title ?>"><i class="file fas fa-file-alt col-2"></i><span class="col-7"><?= $post->post_title ?></span><i class="download ti-download col-3"></i></a>
				</li>
	        <?php endforeach; ?>
	        <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
		</ul>
    <?php endif; ?>

	<h2>Plaquettes et livrets</h2>
	<?php
    $posts = get_field('plaquettes_livrets');

    if( $posts ): ?>
		<ul class="row">
	        <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
				<?php
					setup_postdata($post);
					$file = get_field('url');
				?>
				<li class="link-pdf col-12 col-lg-4">
					<a class="d-flex align-items-center" href="<?= $file['url'] ?>" title="Télécharger le fichier : <?= $post->post_title ?>"><i class="file fas fa-file-alt col-2"></i><span class="col-7"><?= $post->post_title ?></span><i class="download ti-download col-3"></i></a>
				</li>
	        <?php endforeach; ?>
	        <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
		</ul>
    <?php endif; ?>

	<h2>Dossiers et guides</h2>
	<?php
    $posts = get_field('dossiers_guides');

    if( $posts ): ?>
		<ul class="row">
	        <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
				<?php
					setup_postdata($post);
					$file = get_field('url');
				?>
				<li class="link-pdf col-12 col-lg-4">
					<a class="d-flex align-items-center" href="<?= $file['url'] ?>" title="Télécharger le fichier : <?= $post->post_title ?>"><i class="file fas fa-file-alt col-2"></i><span class="col-7"><?= $post->post_title ?></span><i class="download ti-download col-3"></i></a>
				</li>
	        <?php endforeach; ?>
	        <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
		</ul>
    <?php endif; ?>
</div>

	<?php endwhile; endif; wp_reset_query();?>

<?php get_footer();?>
