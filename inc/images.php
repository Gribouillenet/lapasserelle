<?php
//IMAGE À LA UNE
add_theme_support( 'post-thumbnails' );

//IMAGES RESPONSIVE
function responsive_images( $html ) {
  $html = preg_replace( '/(width|height)="\d*"\s/', "", $html );
  return $html;
}
add_filter( 'post_thumbnail_html', 'responsive_images', 10 );
add_filter( 'image_send_to_editor', 'responsive_images', 10 );
add_filter( 'wp_get_attachment_link', 'responsive_images', 10 );

add_filter('wp_calculate_image_srcset_meta', '__return_null');

//NOUVELLE TAILLE D'IMAGE
function new_format_image(){
	add_image_size( '12col', 2540, 1050, true );
	add_image_size( '4col', 736, 736, true );
    add_image_size( '2col', 368, 368, true );

}
add_action( 'after_setup_theme', 'new_format_image' );
