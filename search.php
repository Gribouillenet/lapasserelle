<?php get_header();

	if ( have_posts() ) :

	$url_fond = get_stylesheet_directory_uri() . '/img/svg/top-blc.svg';

?>
	<div id="section-primary" class="section-primary clearfix">
		<h1 class="container-fluid corner-round corner-round-bibliographie title-page col-12">Résultats de votre recherche</h1>
	</div>

	<div id="section-secondary" class="section-secondary container clearfix">
		<h3><?php printf( __( 'Pour le ou les mot(s) : %s' ), '<span class="words-resluts">' . esc_html( get_search_query() ) . '</span>' ); ?></h3>
		<div class="content-single container-column content-search clearfix">
			<ul>
				<?php while ( have_posts() ) : the_post();?>
					<?php get_template_part( 'template-part/content', 'search' );?>
				<?php endwhile;?>
			</ul>
		</div>
	</div>

	<?php else :?>

	<div id="primary-section" class="section-primary clearfix">
		<h1 class="container-fluid corner-round corner-round-bibliographie title-page col-12"><?php _e( 'Nothing Found', 'twentysixteen' ); ?></h1>
	</div>
	</div>
	<div id="section-secondary" class="section-secondary container clearfix no-results not-found">
			<div class="content-single container-column content-search clearfix">
				<?php get_template_part( 'template-part/content', 'none' );?>
			</div>
	</div>

	<?php endif; ?>

<?php get_footer(); ?>