<?php
// SUPPRESSION DE FONCTIONNALITÉ DU THEME PARENT
function wp_acs_remove_features(){

	// SUPPRESSION SIDEBAR
	add_action( 'widgets_init', 'wp_acs_parent_unregister_widgets', 10 );
	function wp_acs_parent_unregister_widgets()
	{
	    unregister_widget( 'twentysixteen_widgets_init' );
	}
	// SUPPRESSION SCRIPTS JS
	add_action( 'wp_print_scripts', 'child_overwrite_scripts', 100 );
	function child_overwrite_scripts()
	{
	    wp_deregister_script( 'twentysixteen-script' );
    }

}
add_action( 'after_setup_theme', 'wp_acs_remove_features', 10 );

// AJOUT CHAMPS PROFIL UTILISATEUR
/*
add_filter('user_contactmethods','wpm_user_fields',10,1);

function wpm_user_fields( $contactmethods ) {

	$contactmethods['departement'] = 'Département';

	return $contactmethods;
}
*/
//SUPPRESSION DU MENU ARTICLES
function remove_menu_items() {
  global $menu;
  $restricted = array(__('Posts'));
  end ($menu);
  while (prev($menu)){
    $value = explode(' ',$menu[key($menu)][0]);
    if(in_array($value[0] != NULL?$value[0]:"" , $restricted)){
      unset($menu[key($menu)]);}
    }
}
add_action('admin_menu', 'remove_menu_items');


//SUPPRESSION DU MENU COMMENTAIRES
add_action( 'admin_menu' , 'remove_commentstatus_meta_box' );
function remove_commentstatus_meta_box() {
remove_meta_box( 'commentstatusdiv' , 'post' , 'normal' );
}

//AJOUT EXCERPT
add_action('init', 'page_excerpt');
function page_excerpt() {
	add_post_type_support( 'page', 'excerpt' );
}

//MODIFICATION DU TINYMCE
function gn_tinymce_filtre($arr){
    $arr['block_formats'] = 'Paragraph=p;Heading 3=h3;';
    return $arr;
  }
add_filter('tiny_mce_before_init', 'gn_tinymce_filtre');

//AJOUT DE LA COLONNE DATE CONCERT DANS L'ADMIN DEPUIS UN CHAMPS ACF
function grib_columns( $column ){
	unset( $column['date'] );
	$column['date_event'] = 'Date du concert ou spectacle';
	return $column;
}
add_filter( 'manage_concert_spectacle_posts_columns', 'grib_columns' );

//AJOUT D'UNE COLONNE DANS L'ADMIN
function grib_rows( $column, $post_id ){
	$custom_fields = get_post_custom( $post_id );
// 	var_dump($custom_fields);
	switch( $column ) :
	case 'date_event' :
	if( $custom_fields['date_event'][0] != '' ):
	$date = $custom_fields['date_event'][0];
	$dt = new DateTime($date);
	$dt = $dt->format('d/m/Y');
	echo $dt;
      else:
        echo 'Pas de date définie';
      endif;
      break;
    default:
      break;
  endswitch;
}
add_action( 'manage_concert_spectacle_posts_custom_column', 'grib_rows', 10, 2 );