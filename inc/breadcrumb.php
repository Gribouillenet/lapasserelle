<?php
//BREADCRUMB
function the_breadcrumb() {

   	echo '<ul class="d-flex"><li class="return-home"><span class="breadcrumb-text-before">Vous êtes ici :</span> '; // BREADCRUMB NAVIGATION

	if (is_page() && !is_front_page() || is_single() || is_singular() || is_category() || is_archive() ) {

		echo '<a title="Revenir à l\'Accueil" rel="nofollow" href="/"><i class="fas fa-home"></i><span>Accueil</span></a></li>';

		/*
		if (is_page()) {
		$ancestors = get_post_ancestors($post);

		if ($ancestors) {
		$ancestors = array_reverse($ancestors);

			 foreach ($ancestors as $crumb) {
			 echo '<li> > <a href="'.get_permalink($crumb).'" > '.get_the_title($crumb).'</a></li>';
			 }
		 }
		}
		*/

		//single post-types
		if (is_single()) {
			global $post;
			if( $post && $post->post_type ){
				$obj = $post->post_type;
				echo '<li> <a href="/' . $obj . '"> > ' . $obj . '</a></li>';
			}
		 }
		//single category
		elseif (is_single()) {
			 $category = get_the_category();
			 echo '<li> <a href="'.get_category_link($category[0]->cat_ID).'"> > '.$category[0]->cat_name.'</a></li>';
		}

		if (is_archive()) {
			$archive = post_type_archive_title( '', false );

			 echo '<li> > '. $archive .'</li>';
		}

		if (is_category()) {
		 $category = get_the_category();
		 echo '<li> > '.$category[0]->cat_name.'</li>';
		}

		// Current page
		if (is_page() || is_single()) {
			echo '<li class="breadcrumb-current-page"> > '.get_the_title().'</li>';
		}
		echo '</ul>';
		}
		 /* //FRONT PAGE
			elseif (is_front_page()) {
			echo '<ul>';
			echo '<li><a title="Accueil - NOM DU SITE" rel="nofollow" href="http://VOTRE_SITE.com/">Accueil</a></li>';
			echo '</ul>';
		}*/
	}