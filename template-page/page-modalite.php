<?php
/*
Template Name: modalités d'accueil
*/

get_header();
?>

<div id="section-primary" class="section-primary clearfix">

	<?php if(have_posts()) : while(have_posts()) : the_post();

    $id = get_the_ID();
	$size = '12col';
    $images = get_the_post_thumbnail($id, $size);
    $url_image = get_the_post_thumbnail_url($id, $size);
	$url_overlay = get_stylesheet_directory_uri() . '/img/png/overlay-pink.png';
    $url_fond = get_stylesheet_directory_uri() . '/img/svg/top-pink.svg';

    $title_page = get_field('title_page');
    $contenu = get_the_content();

    ?>

	<h1 class="container-fluid title-page corner-round corner-round-modalites" data-image="<?= $url_image ;?>" data-overlay="<?= $url_overlay ;?>" data-fond="<?= $url_fond ;?>"><?= $title_page ?></h1>

</div>

<div id="section-secondary-modalites" class="section-secondary-modalites clearfix">
    <div class="contenu container">
		<?= $contenu ?>
	</div>
</div>

<div id="section-tertiary" class="section-tertiary container clearfix">
    <?php
    if( have_rows('modalite') ):
        while ( have_rows('modalite') ) : the_row();
        ?>
            <?php get_template_part('template-part/content', 'groupe'); ?>
        <?php endwhile;

    else :
    ?>
        <div>
            <h3>Merci de bien ajouter une modalitée à la page.</h3>
        </div>
    <?php
    endif;
    ?>
</div>

	<?php endwhile; endif; wp_reset_query(); ?>

<?php get_footer();?>
