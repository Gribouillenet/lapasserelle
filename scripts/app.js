$(function(){
//IMAGES
    if( $( 'img' ).length ){
	    $( 'img' ).removeAttr( 'Width' ).removeAttr( 'Height' ).removeAttr( 'sizes' );
    }

//GESTION DU MENU EN FONCTION DE LA TAILLE D'ÉCRAN
//JUSQU'À 992px
if ($ ( window ).width() < 992 ){
	if ( $('#toggle-menu').length ){
		$('#toggle-menu').click(function(){
			$(this).toggleClass('toggled-on');
			$('#site-navigation').toggleClass('toggled-on');
			return false;
		});
	}
}
//À PARTIR DE 992px
if ($ ( window ).width() >= 992 ){

    //GESTION DE LA WIDTH DU MENU

    if ( $( '.site-navigation' ).length ){

        if($('.sub-menu').length){
            $('.sub-menu').parent().addClass('nav-dir');

            $('.primary-nav li:nth-child(1)').addClass('nav-dir-first');
            $('.nav-dir-first .sub-menu').addClass('sub-menu-first');

            $('.primary-nav li:nth-child(2)').addClass('nav-dir-second');
            $('.nav-dir-second .sub-menu').addClass('sub-menu-second');

        }
    /*    $('.sub-menu:nth-child(1)').addClass('sub-menu-first');*/
        $('.nav-dir ul:nth-child(1)').addClass('sub-menu-second');


        var WidthScreen = $(window).width();
        var WidthHeader = $('.header-container').innerWidth();
        var WidthLogo = $('.branding-site').innerWidth();
        var WidthItemnav = $('.nav-dir:nth-child(1)').innerWidth();
        var Calc = WidthScreen - WidthHeader;
        var MarginCalc = Calc / 2;
        var Margin_first = MarginCalc + WidthLogo + 67;
        //var Margin_first = MarginCalc + WidthLogo + 35;
        var Margin_second = MarginCalc + WidthLogo + WidthItemnav + 67;
        var MarginLeft_first = '-' + Margin_first + 'px';
        var MarginLeft_second = '-' + Margin_second + 'px';
        var MarginLeft_menu = Margin_first + 'px';
        var styles_first = {
            "width": WidthScreen,
            "margin-left": MarginLeft_first
        }
        var styles_second ={
            "width": WidthScreen,
            "margin-left": MarginLeft_second
        }
        var styles_menu ={
            "margin-left": MarginLeft_menu
        }
        console.log(WidthScreen);
        console.log(WidthHeader);
		console.log(MarginLeft_first);
            /*
        console.log(WidthItemnav);
        /*
        console.log( 'Margin DIV Navigations : ' + Margin );
        console.log($('.site-main-header').innerWidth());
        console.log('width screen : ' + WidthScreen);
        console.log('width logo : ' + WidthLogo);
        console.log('Margin DIV Navigations : ' + MarginNav);
        console.log('Padding header : ') + PaddingNav;
        console.log('Margin finale : ' + MarginLeft);
*/
        $('.sub-menu-first').css(styles_first);
        $('.menu-item-50, .menu-item-56').css(styles_menu);
        $('.sub-menu-second').css(styles_second);
    }
}

//POPUP
    if( $( '.popup-move' ).length ){
        $(' .popup-move ').magnificPopup({
            type: "inline",
            fixedContentPos: false,
            fixedBgPos: true,
            overflowY: "auto",
            closeBtnInside: true,
            preloader: false,
            midClick: true,
            removalDelay: 300,
        });
    }
//SLICK
    if( $( '.slider' ).length ){
        $( '.slider-container' ).slick({
            autoplay: true,
            arrows: false,
            fade: false,
            dots: true,
            appendDots: $( '.slick-navigation' ),
            autoplaySpeed: 5000,
            easing: 'swing',
            speed: 1000,
            zIndex: 10
        });
    }
//SCROLL
    if( $( '.section_chiffre' ).length ){
        $( '.counter' ).counterUp({
            delay: 30,
            time: 2000
        });
        /*
        var position_div = $('.section_chiffre').position().top;

        $(window).scroll(function() {
            if ($(this).scrollTop() > position_div ) {
                $('.counter').addClass('animated fadeInDownBig');
            }
        });
        */
    }
//GESTION DES IMAGES D'EN-TÊTE
    if($('.corner-round-figure').length ){

		var img = $('.corner-round-figure').attr('data-image');
        var bg = $('.corner-round-figure').attr('data-fond');

		$('.corner-round-figure').css( 'background-image', 'url(' + bg + '), url(' + img + ')');
	}
    if($('.corner-round-modalites').length ){

		var img = $('.corner-round-modalites').attr('data-image');
        var ol = $('.corner-round-modalites').attr('data-overlay');
        var bg = $('.corner-round-modalites').attr('data-fond');

		$('.corner-round-modalites').css( 'background-image', 'url(' + bg + '), url(' + ol + '), url(' + img + ')');
	}
//GESTION AFFICHAGE ET ICON WIDGET TÉLÉPHONE
	if( $('.widget-tel').length ){
		$('.widget-tel .textwidget').append('<i class="fas fa-mobile-alt"></i>');
		$('.widget-tel').addClass('d-flex align-items-center justify-content-center col-6 col-lg-12 p-0');
	}
//GESTION ZONE DE RECHECHE
	if($('.widget-search').length){
		$('.widget-search a').click(function(e){
			e.preventDefault();
			$('.widget-search').toggleClass('search-on');
			$('.search-container').toggleClass('search-on');
			$('.overlay').toggleClass('overlay-on');
		});
	}
});
