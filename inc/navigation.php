<?php
// AJOUT MENU
register_nav_menus( array(
	'menu-copyright' => __( 'Menu copyright' ),
	'menu-footer' => __( 'Menu footer' )
) );

//AJOUTER CLASSE SUR LI MENU
function special_nav_class($classes, $item) {
	$slug = strtolower($item->title);
    $slug = str_replace(' ?', '', $slug);
    $slug = str_replace('<span>', '-', $slug);
    $slug = str_replace('</span>', '', $slug);
    $slug = str_replace(' !', '', $slug);
    $slug = str_replace(' ', '-', $slug);
    $slug = preg_replace('#è|é|ê|ë#', 'e', $slug);
	$classes[] = $slug;
	return $classes;
}
add_filter('nav_menu_css_class' , 'special_nav_class' , 10 , 2);


//AJOUT D'UNE IMAGE AU MENU
add_filter('wp_nav_menu_objects', 'my_wp_nav_menu_objects', 10, 2);
function my_wp_nav_menu_objects( $items, $args ) {
	foreach( $items as &$item ) {
		$image = get_field('image', $item);
		if( $image ) {
// 			var_dump($item);
			$item->title .= '<div class="container-menu-image"><img class="img-thumb-link thumb-link-'. $item->ID .'" src="'.$image['url'].'" alt="'.$image['url'].'"/></div>';
		}
	}
	return $items;
}
