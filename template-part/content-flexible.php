<?php

// check if the flexible content field has rows of data
if( have_rows('zones') ):

     // loop through the rows of data
    while ( have_rows('zones') ) : the_row();

        $num_mission = get_field('subtitle_page');
        $num_mission = str_replace(' ', '-', $num_mission);
        $num_mission = strtolower($num_mission);

        $title = get_sub_field('title');
        $paragraphe = get_sub_field('paragraphe');
        $paragraphe_1col = get_sub_field('paragraphe_1col');
        $paragraphe_2col = get_sub_field('paragraphe_2col');
        $adresse = get_sub_field('bouton_adresse');
        $tel = get_sub_field('bouton_tel');
        $contact = get_sub_field('bouton_contact');
        ?>

        <?php if( get_row_layout() == 'title_paragraphe_1col' ):?>

        	<div class="container section-content">
                <div class="title-paragraphe-1col">
                    <div class="<?= $num_mission ?>">
                        <h2><?= $title ?></h2>
                        <?= $paragraphe ?>
                    </div>
                </div>
        	</div>

        <?php elseif( get_row_layout() == 'title_paragraphe_2col' ):?>

        	<div class="container section-content">
               <div class="title-paragraphe-2col">
                    <div class="<?= $num_mission ?>">
                        <h2><?= $title ?></h2>

                        <div class="row">
                            <div class="col-12 col-md-6">
                                <?= $paragraphe_1col ?>
                            </div>
                            <div class="col-12 col-md-6">
                                <?= $paragraphe_2col ?>
                            </div>
                        </div>
                   </div>
               </div>
           </div>

        <?php elseif( get_row_layout() == 'title_paragraphe_1col_gris' ):?>

        	<div class="section-contact fond-gris">
                <div class="title-paragraphe-1col-gris container">
                    <div class="<?= $num_mission ?>">
                        <h2><?= $title ?></h2>
                        <?= $paragraphe ?>

                        <?php if ( $adresse || $tel || $contact ) : ?>
                            <div class="boutons row d-flex justify-content-center">
                                <?php if ( $adresse ) : ?>
                                    <div class="col-12 col-md-6">
                                        <div class="d-flex align-items-center justify-content-center adresse">
                                            <span><?= $adresse ?></span><i class="ml-3 fas fa-map-marker-alt"></i>
                                        </div>
                                    </div>
                                <?php endif; ?>
                                <?php if ( $tel ) : ?>
                                    <div class="col-12 col-md-3">
                                        <div class="d-flex align-items-center justify-content-center tel">
                                            <a href="tel:<?= $tel ?>" title="Appelez-nous" target="_blank"><?= $tel ?></a><i class="ml-3 fas fa-mobile-alt"></i>
                                        </div>
                                    </div>
                                <?php endif; ?>
                                <?php if ( $contact ) : ?>
                                    <div class="col-12 col-md-3">
                                        <a class="d-flex align-items-center justify-content-center contact" href="/contact"><span><?= $contact ?></span><i class="ml-3 fab fa-telegram-plane"></i></a>
                                    </div>
                                <?php endif; ?>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
        	</div>

        <?php elseif( get_row_layout() == 'title_paragraphe_2col_gris' ):?>

        	<div class="section-contact fond-gris">
        	    <div class="title-paragraphe-2col-gris container">
                    <div class="<?= $num_mission ?>">
                        <h2><?= $title ?></h2>

                        <div class="row">
                            <div class="col-12 col-md-6">
                                <?= $paragraphe_1col ?>
                            </div>
                            <div class="col-12 col-md-6">
                                <?= $paragraphe_2col ?>
                            </div>
                        </div>
                        <?php if ( $adresse || $tel || $contact ) : ?>
                            <div class="boutons row d-flex justify-content-center">
                                <?php if ( $adresse ) : ?>
                                    <div class="col-12 col-md-6">
                                        <div class="d-flex align-items-center justify-content-center adresse">
                                            <span><?= $adresse ?></span><i class="ml-3 fas fa-map-marker-alt"></i>
                                        </div>
                                    </div>
                                <?php endif; ?>
                                <?php if ( $tel ) : ?>
                                    <div class="col-12 col-md-3">
                                        <div class="d-flex align-items-center justify-content-center tel">
                                            <a href="tel:<?= $tel ?>" title="Appelez-nous" target="_blank"><?= $tel ?></a><i class="ml-3 fas fa-mobile-alt"></i>
                                        </div>
                                    </div>
                                <?php endif; ?>
                                <?php if ( $contact ) : ?>
                                    <div class="col-12 col-md-3">
                                        <a class="d-flex align-items-center justify-content-center contact" href="/contact"><span><?= $contact ?></span><i class="ml-3 fab fa-telegram-plane"></i></a>
                                    </div>
                                <?php endif; ?>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
        	</div>

        <?php
        endif;

    endwhile;

else :
?>
    <div>
        <h3>Merci de bien ajouter une zone à la page.</h3>
    </div>

<?php
endif;

?>
