<?php get_header(); ?>

<div id="section-primary" class="section-primary clearfix">

	<?php if(have_posts()) : while(have_posts()) : the_post();

	$url_fond = get_stylesheet_directory_uri() . '/img/svg/top-blc.svg';

	?>

	<h1 class="container-fluid corner-round corner-round-bibliographie title-page col-12">Bibliographie</h1>

</div>

<div id="section-secondary" class="section-secondary container clearfix">

	<?php
    $file = get_field('url');
    ?>
    <div class="container d-flex justify-content-center link-pdf col-12 col-md-5">
    	<a class="d-flex align-items-center" href="<?= $file['url'] ?>" title="Télécharger le fichier : <?php the_title(); ?>"><i class="file fas fa-file-alt col-2"></i><span class="col-7"><?php the_title(); ?></span><i class="download ti-download col-3"></i></a>
    </div>

    <div class="container d-flex justify-content-center button-return">
        <a class="d-flex align-items-center" href="/la-bibliographie" title="Voir toutes les bibliographies"><i class="undo fas fa-undo col-3"></i><span class="col-9">Voir toutes les bibliographies</span></a>
    </div>
</div>

	<?php endwhile; endif; wp_reset_query();?>

<?php get_footer();?>
