<?php
//
// PAGE LOGIN
//
function login_enqueue_scripts(){
echo '
<div></div>
<style type="text/css" media="screen">
body{ background:#FFFFFF !important; }
.background-cover{
background:#E8DED0;
-webkit-background-size: cover;
-moz-background-size: cover;
-o-background-size: cover;
background-size: cover;
position:fixed;
top:0;
left:0;
z-index:10;
overflow: hidden;
width: 100%;
height:100%;
}
#login{ z-index:9999; position:relative; }
.login form { box-shadow: 0px 0px 0px 0px !important; }
.login h1 a {
background:url('.get_bloginfo('stylesheet_directory').'/img/login/logo.png) no-repeat center top !important;
background-size: 100% !important;
height:110px !important;
max-height:110px !important;
max-width:320px !important;
width: 320px !important;
}
input.button-primary, button.button-primary, a.button-primary{
border-radius: 3px !important;
background:url('.get_bloginfo('stylesheet_directory').'/img/login/button.jpg);
border:none !important;
font-weight:normal !important;
text-shadow:none !important;
}
.wp-core-ui .button-primary{
	background:#4576AD !important;
	border-color:#4576AD !important;
	box-shadow:none !important;
	-webkit-transition: all 0.3s ease-in-out;
	-moz-transition: all 0.3s ease-in-out;
	-ms-transition: all 0.3s ease-in-out;
	-o-transition: all 0.3s ease-in-out;
	transition: all 0.3s ease-in-out;
}
.wp-core-ui .button-primary:hover{
	background:#2D2F3F !important;
	border-color:#2D2F3F !important;
}
.login .message{
	border-left: 4px solid #3E4342 !important;
}
.button:active, .submit input:active, .button-secondary:active {
background:#2D2F3F !important;
text-shadow: none !important;
}
.login #nav a, .login #backtoblog a {
color:#fff !important;
text-shadow: none !important;
}
.login #nav a:hover, .login #backtoblog a:hover{
color:#2D2F3F !important;
text-shadow: none !important;
}
.login #nav, .login #backtoblog{
text-shadow: none !important;
}
#nav{
	display:none;
}
</style>
';
}
add_action( 'login_enqueue_scripts', 'login_enqueue_scripts' );