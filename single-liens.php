<?php get_header(); ?>

<div id="section-primary" class="section-primary clearfix">

	<?php if(have_posts()) : while(have_posts()) : the_post();

	$url_fond = get_stylesheet_directory_uri() . '/img/svg/top-blc.svg';

	$title_page = get_the_title();
	?>

	<h1 class="container-fluid corner-round corner-round-liens title-page col-12">Lien partenaire</h1>

</div>

<div id="section-secondary" class="section-secondary container clearfix">

	<?php
    $bool = get_field('plus_lien');
    $file = get_field('site_web_du_partenaire');
    $files = get_field('sites_web_du_partenaire');

    $size = '4col';
    $contenu = get_the_content();
    $title = get_the_title();

    if ($bool) {
    ?>
        <div class="container link-site col-12 col-lg-4">
            <figure><?= the_post_thumbnail($size) ?></figure>
            <?= $contenu ?>
            <a class="d-flex align-items-center col-12 col-lg-10" href="<?= $file ?>" title="Voir le site : <?= $title ?>"><i class="world ti-world col-3"></i><span class="col-9"><?= $title ?></span></a>
        </div>
    <?php
    } else {
    ?>
        <div class="link-sites col-12">
            <div class="row">
                <div class="col-12">
                    <figure><?= the_post_thumbnail($size) ?></figure>
                    <?= $contenu ?>
                </div>
                <div class="d-flex row col-12">
                    <?php foreach( $files as $file):
                        $patterns = array();
                        $patterns[0] = 'http://';
                        $patterns[1] = 'https://';
                        $patterns[2] = 'www.';
                        $patterns[3] = '/';
                        $file_link = str_replace($patterns, '', $file['url_site']);
                        ?>
                        <a class="d-flex align-items-center col-12 col-lg-5" href="<?= $file['url_site'] ?>" title="Voir le site : <?= $title ?>"><i class="world ti-world col-2"></i><span class="col-9"><?= $file_link ?></span></a>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    <?php } ?>

    <div class="container d-flex justify-content-center button-return">
        <a class="d-flex align-items-center" href="/les-liens-utiles" title="Voir tous les liens"><i class="undo fas fa-undo col-3"></i><span class="col-9">Voir tous les liens</span></a>
    </div>
</div>

	<?php endwhile; endif; wp_reset_query();?>

<?php get_footer();?>
