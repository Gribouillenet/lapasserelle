<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php endif; ?>
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<!--<?php include_once("analyticstracking.php") ?>-->
	<header id="header-site" class="header-site" role="banner">
        <div class="header-container container">
	        <div class="row justify-content-between">
	            <div class="branding-site-container col-md-5 col-lg-3">
		            <div class="branding-site d-flex row align-items-center justify-content-center">
						<a class="logo-site col-6 col-md-8 p-0" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
		                   <?php
		                    $site_name = get_bloginfo( 'name', 'display' );
		                    $site_name = str_replace(' ', '<span>', $site_name);
		                    echo $site_name . '</span>'; ?>
		                </a>
					</div>
	            </div>
				<div class="container-menu d-flex flex-column d-lg-none justify-content-center align-items-md-end">
					<p class=" toggle-menu-text">Menu</p>
		            <button id="toggle-menu" class="toggle-menu">
						<span>Navigation</span>
					</button>
				</div>

				<?php if ( has_nav_menu( 'primary' ) ) : ?>

					<nav id="site-navigation" class="site-navigation d-flex align-items-lg-center justify-content-center col-lg-7" role="navigation" aria-label="<?php esc_attr_e( 'Primary Menu', 'twentysixteen' ); ?>">
						<?php
							wp_nav_menu( array(
								'theme_location' => 'primary',
								'menu_class'     => 'primary-nav dropdown',
							 ) );
						?>
					</nav>

				<?php endif; ?>

				<div id="widgets-site" class="widgets-site d-flex d-lg-block col-lg-2 p-0 pl-mlg-3 pr-lg-0 clearfix">
					<div class="d-flex col-6 col-lg-12 p-0 container-widget">
						<div class="btn-widget widget-contact col-6">
							<a href="/contact">
								<i class="fab fa-telegram-plane">
									<span class="d-block">Contactez-nous</span>
								</i>
							</a>
						</div>
						<div class="btn-widget widget-search col-6">
							<a href="#0" title="Vous recherchez ?">
								<i class="ti-search">
									<span class="d-block">Ouvrir la recherche</span>
								</i>
							</a>
						</div>
					</div>
					<?php dynamic_sidebar( 'sidebar-tel' ); ?>
				</div>
	        </div>
        </div>
        <div class="search-container">
			<?php include 'inc/searchform.php';?>
		</div>
		<div class="overlay"></div>
	</header><!--.siter-header-->


	<main id="main" class="site-main clearfix" role="main">
