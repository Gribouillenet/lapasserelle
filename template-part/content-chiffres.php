<div class="d-flex">
    <div class="row">
        <?php
        if( have_rows('chiffres') ):

            while ( have_rows('chiffres') ) : the_row();

                $nombre = get_sub_field('nb');
                $texte = get_sub_field('txt');
                ?>

                <div class="bloc_chiffre col-12 col-md-4">
                    <span class="counter"><?= $nombre ?></span>
                    <p><?= $texte ?></p>
                </div>

                <?php
            endwhile;
        endif;
        ?>
    </div>
</div>
