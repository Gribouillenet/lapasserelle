<?php get_header(); ?>

<section id="primary" class="content-primary container">

	<?php if(have_posts()) : while(have_posts()) : the_post();

			the_content();

	endwhile; endif; wp_reset_query();?>

</section>

<?php get_footer(); ?>