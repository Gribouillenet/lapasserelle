<?php
//
// Recommended way to include parent theme styles.
//  (Please see http://codex.wordpress.org/Child_Themes#How_to_Create_a_Child_Theme)
//
add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
function theme_enqueue_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array('parent-style')
    );
}
// JQUERY
require_once('inc/jquery.php');

// PERSONNALISAITON DE LA PAGE DE LOGIN
require_once('inc/login.php');

// PERSONNALISATION DU DASHBOARD
require_once('inc/dashboard.php');

// SÉCURITÉ WORDPRESS
require_once('inc/security.php');

// SUPPRESSION DE CERTAINES FONCTIONNALITÉS DU THÈME ENFANT ET PARENT
require_once('inc/tunning.php');

// COMMENTAIRES
require_once('inc/comments.php');

// GESTION DES IMAGES
require_once('inc/images.php');

// GESTION DES NAVIGATIONS
require_once('inc/navigation.php');

// GESTION DES WIDGETS
require_once('inc/widgets.php');

// GESTION DES CLASS SLUG
require_once('inc/slug.php');

// GESTION DU FORMATAGE DES PARAGRAPHES
// require_once('inc/formating-content.php');

// GESTION DU BREADCRUMB
// require_once('inc/breadcrumb.php');

// GOOGLE MAP
// require_once('inc/google-map.php');

// PERSONNALISATION DE RECHERCHE
// require_once('inc/search.php');

// BOUTON DE PARTAGE
// require_once('inc/share.php');

// GESTION DES BLOCS AVEC ACF
// require_once('inc/register-block.php');
