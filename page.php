<?php get_header();?>

<div id="section-primary" class="section-primary clearfix">

	<?php if(have_posts()) : while(have_posts()) : the_post();

	$url_fond = get_stylesheet_directory_uri() . '/img/svg/top-blc.svg';

	$title_page = get_the_title();

	$content_page = get_the_content();
	?>

	<h1 class="container-fluid corner-round corner-round-bibliographie title-page col-12"><?= $title_page ?></h1>

</div>
	<div id="section-secondary" class="section-secondary container clearfix">

		<?php

			if( $content_page ){

				echo $content_page;
			}



			if( is_page('387') ){
				echo do_shortcode( '[contact-form-7 id="390" title="contact"]' );
			}



			if( is_page('377') ) :

				$args = array(
					'exclude' => '377, 379',
					'title_li' => ''
				);
			?>

			<ul class="site-map-container"><?php wp_list_pages($args);?></ul>
			<?php endif;?>

	</div>
	<?php endwhile; endif;?>


<?php get_footer();?>
