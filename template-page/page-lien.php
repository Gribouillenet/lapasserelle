<?php
/*
Template Name: liens utiles
*/

get_header();
?>

<div id="section-primary" class="section-primary clearfix">

	<?php if(have_posts()) : while(have_posts()) : the_post();

	$url_fond = get_stylesheet_directory_uri() . '/img/svg/top-blc.svg';

	$title_page = get_the_title();
	?>

	<h1 class="container-fluid corner-round corner-round-liens title-page col-12"><?= $title_page ?></h1>
</div>

<div id="section-secondary" class="section-secondary container clearfix">

	<h2>Pour votre curiosité, à consulter</h2>
	<?php get_template_part('template-part/content-link', 'a-consulter'); ?>

	<h2>Les Engagements fédératifs de l’association</h2>
	<?php get_template_part('template-part/content-link', 'les-engagements-association'); ?>

	<h2>Les sites officiels</h2>
	<?php get_template_part('template-part/content-link', 'sites-officiels'); ?>

	<h2>Les partenaires</h2>
	<?php get_template_part('template-part/content-link', 'les-partenaires'); ?>

</div>

	<?php endwhile; endif; wp_reset_query();?>

<?php get_footer();?>
