<?php
/*
Template Name: missions
*/

get_header();
?>

<div id="section-primary" class="section-primary clearfix">

	<?php if(have_posts()) : while(have_posts()) : the_post();

    $id = get_the_ID();
    $size = '4col';
    $images = get_the_post_thumbnail($id, $size);

    $num_mission_old = get_field('subtitle_page');
    $num_mission = str_replace(' ', '-', $num_mission_old);
    $num_mission = strtolower($num_mission);
    $title_page = get_field('title_page');
    $intro = get_the_content();

    ?>

    <div class="fond-<?= $num_mission ?> corner-round corner-round-mission">
        <div class="d-flex container">
            <div class="row justify-content-center justify-content-lg-start align-items-lg-center">
                    <figure class="icon-mission col-8 col-lg-4"><?= $images ?></figure>
                <div class="title-page col-12 col-lg-8">
<!--                     <h4><?= $num_mission_old ?></h4> -->
                    <h1><?= $title_page ?></h1>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container section-intro">
    <?= $intro ?>
</div>

<?php get_template_part('template-part/content', 'flexible'); ?>

	<?php endwhile; endif; wp_reset_query(); ?>

<?php get_footer();?>
