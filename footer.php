	</main><!-- .site-main -->

	<footer id="section-footer" class="site-footer" role="contentinfo">

		<?php if ( has_nav_menu( 'menu-copyright' ) ) : ?>

		<div class="footer-navigation container d-flex">
			<nav class="pages-footer-navigation" role="navigation">
				<?php
					wp_nav_menu( array(
						'theme_location' => 'menu-footer',
						'menu_class'     => 'footer-links-menu d-flex align-items-end',
						'depth'          => 1,
						'link_before'    => '<span class="link-footer">',
						'link_after'     => '</span>',
					) );
				?>
			</nav><!-- .copyright-navigation -->

			<nav class="copyright-navigation" role="navigation">
				<?php
					wp_nav_menu( array(
						'theme_location' => 'menu-copyright',
						'menu_class'     => 'copyright-links-menu d-flex align-items-end',
						'depth'          => 1,
						'link_before'    => '<span class="link-copyright">',
						'link_after'     => '</span>',
					) );
				?>
			</nav><!-- .copyright-navigation -->

		</div>

		<?php endif; ?>

	</footer><!-- .site-footer -->
<?php wp_footer(); ?>
</body>
</html>
