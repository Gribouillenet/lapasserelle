<?php get_header(); ?>
<div id="section-primary" class="section-primary clearfix">

   <?php if(have_posts()) : while(have_posts()) : the_post();

    $id = get_the_ID();
    $size = 'full';
    $images = get_the_post_thumbnail($id, $size);
    $url_image = get_the_post_thumbnail_url($id, $size);
    $url_fond = get_stylesheet_directory_uri() . '/img/svg/top-blc.svg';
    $url_fond_mobile = get_stylesheet_directory_uri() . '/img/svg/top-blc.svg';

    $title_page = get_the_title();
    $intro = get_the_excerpt();
    $contenu = get_the_content();

    ?>
    <div class="container-fluid corner-round corner-round-figure" data-image="<?= $url_image ;?>" data-fond="<?= $url_fond ;?>" data-fond-mobile="<?= $url_fond_mobile ;?>">
        <div class="container">
            <div class="container-intro row justify-content-lg-end">
                <div class="col-12 col-lg-7">
                    <h1><?= $title_page ?></h1>
                    <p><?= $intro ?></p>
                </div>
            </div>
            <div class="row justify-content-lg-end">
	            <div class="col-12 col-lg-7 d-md-flex">
		            <a class="direct-link link_1 popup-move d-flex align-items-center justify-content-center col-8 col-md-4" href="#savoir_plus"><span>En savoir plus</span></a>
					<a class="direct-link link_2 d-flex lign-items-center justify-content-center col-8 col-md-5" href="/contact"><span>Contactez-nous</span><i class="ml-3 fab fa-telegram-plane"></i></a>
	            </div>

            </div><!-- .row -->
        </div>
    </div>
</div>

<!-- Popup itself -->
<div id="savoir_plus" class="white-popup mfp-with-anim mfp-hide">
    <?= $contenu ?>
</div>

<div id="section-secondary" class="section-secondary container clearfix">
    <?php
    $posts = get_field('missions_remoute');

    if( $posts ): ?>

            <div class="row justify-content-between">
                <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
                    <?php
                        setup_postdata($post);
                        $size2 = '2col';
                        $intro = get_field('intro_page_daccueil', false, false);
                        $num_mission = get_field('subtitle_page');
                        $num_mission = str_replace(' ', '_', $num_mission);
                        $num_mission = strtolower($num_mission);
                    ?>
                    <div class="intro-home-mission <?= $num_mission ?> col-12 col-md-5">
                        <figure class="col-6"><?php the_post_thumbnail($size2) ?></figure>
                        <h2><?php the_field('title_page'); ?></h2>
                        <?= $intro ?>
                        <a class="d-flex align-items-center justify-content-center link col-12 col-lg-5" href="<?php the_permalink(); ?>">En savoir plus</a>
                    </div>
                <?php endforeach; ?>
            </div>

        <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
    <?php endif; ?>
</div>

<div id="section-tertiary" class="section-tertiary section_chiffre clearfix">
    <div class="container">
        <h2>La passerelle en chiffres</h2>
        <?php get_template_part('template-part/content','chiffres'); ?>
    </div>
</div>

<div id="section-quaternary" class="section-quaternary section_temoignage clearfix">
    <div class="slider container">
        <h2>Témoignages</h2>
        <?php get_template_part('template-part/content','slider'); ?>
    </div>
</div>

	<?php endwhile; endif; wp_reset_query(); ?>

<?php get_footer(); ?>
