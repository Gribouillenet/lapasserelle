<div class="container">
	<form role="search" method="get" class="search-form row align-items-center p-3 p-md-0 pl-md-3 pt-md-3" action="<?php echo esc_url( home_url( '/' ) ); ?>">
			<input type="search" class="search-field" placeholder="<?php echo esc_attr_x( 'Ici votre recherche...', 'placeholder'); ?>" value="<?php echo get_search_query(); ?>" name="s" />
			<button type="submit" class="submit-search">Ok</button>
	</form>
</div><!-- .container -->
