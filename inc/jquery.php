<?php function custom_enqueue_script() {
		wp_deregister_script('jquery');
		wp_register_script('jquery','https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js', false, '');
		wp_register_script('waypoints','http://cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.3/waypoints.min.js', false, '');
		wp_register_script('counterup','https://cdnjs.cloudflare.com/ajax/libs/Counter-Up/1.0.0/jquery.counterup.min.js', false, '');
		/*wp_enqueue_script( 'google-map', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyALD9qhEu9x7Bm70LsIadZ1nP1i_DNXBew', array(), '3', true );*/

    $js_directory = get_bloginfo( 'stylesheet_directory' ) . '/scripts/';
		wp_register_script( 'app', $js_directory . 'app.js', 'jquery', '1.0');
		wp_register_script( 'slick', $js_directory . 'slick.min.js', 'jquery', '1.0');
		wp_register_script( 'cookies', $js_directory . 'Tag_google_analytics.js', 'jquery', '1.0');
		wp_register_script( 'map', $js_directory . 'google-map.js', 'jquery', '1.0');
		wp_register_script( 'popup', $js_directory . 'jquery.magnific-popup.min.js', 'jquery', '1.0');
		wp_register_script( 'paginate', $js_directory . 'jquery.easyPaginate.js', 'jquery', '1.0');
		wp_register_script( 'ui', $js_directory . 'jquery-ui.min.js', 'jquery', '1.0');
		wp_register_script( 'slide', $js_directory . 'responsiveslides.min.js', 'jquery', '1.0');
		wp_register_script( 'easing', $js_directory . 'jquery.easing.1.3.js', 'jquery', '1.0');
		wp_enqueue_script( 'jquery' );
		wp_enqueue_script( 'app');
		wp_enqueue_script( 'slick');
		wp_enqueue_script( 'cookies');
		/*wp_enqueue_script( 'map' );*/
		wp_enqueue_script( 'popup' );
		wp_enqueue_script( 'paginate' );
		wp_enqueue_script( 'ui' );
		wp_enqueue_script( 'slide' );
		wp_enqueue_script( 'easing' );
		wp_enqueue_script( 'waypoints' );
		wp_enqueue_script( 'counterup' );
	}
	add_action( 'wp_enqueue_scripts', 'custom_enqueue_script', 20 );
	function remove_head_scripts() {
		remove_action('wp_head', 'wp_print_scripts');
		remove_action('wp_head', 'wp_print_head_scripts', 9);
		remove_action('wp_head', 'wp_enqueue_scripts', 1);
		add_action('wp_footer', 'wp_print_scripts', 5);
		add_action('wp_footer', 'wp_enqueue_scripts', 5);
		add_action('wp_footer', 'wp_print_head_scripts', 5);
	}
	add_action( 'wp_enqueue_scripts', 'remove_head_scripts' );
?>
