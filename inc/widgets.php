<?php
//WIDGET
function grib_widgets_init() {
	register_sidebar( array(
		'name'          => 'Téléphone',
		'id'            => 'sidebar-tel',
		'before_widget' => '<div id="%1$s" class="widget %2$s widget-tel container-widget">',
		'after_widget'  => '</div>'
	) );
}
add_action( 'widgets_init', 'grib_widgets_init' );