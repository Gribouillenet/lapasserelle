//SUPPRIMER LES ATTRIBUTS WIDTH, HEIGHT ET SIZES DES IMAGES
$(function(){
	$('img').removeAttr('Width').removeAttr('Height').removeAttr('sizes');
});
//FANCY BOX
$(function(){
	if( $('.fancybox').length ){

		$('.fancybox').fancybox({
			openEffect	: 'none',
			closeEffect	: 'none',
			helpers : {
		        title: {
		            type: 'inside'
		        }
			},
			beforeShow: function () {
	            /* Disable right click */
	            $.fancybox.wrap.bind("contextmenu", function (e) {
	                    return false;
	            });
	        }
		});
	}

	if ( $('.various').length ) {
		$('.various').fancybox({
			scrolling : 'auto',
			padding : 10,
			maxWidth : 800,
			fitToView : true,
			autoSize : true,
			closeClick	: true,
			nextClick : false,
			arrows: false,
			title : 0,
			openEffect	: 'none',
			closeEffect	: 'none'
		});
	}

	if ( $('.btn-lightbox').length ) {
		$('.vc_icon_element-link').fancybox({
			scrolling : 'auto',
			padding : 10,
			maxWidth : 800,
			fitToView : true,
			autoSize : true,
			closeClick	: true,
			nextClick : false,
			arrows: false,
			title : 0,
			openEffect	: 'none',
			closeEffect	: 'none'
		});
	}
});

//CARROUSEL
$(function(){

	if($(".widget-client").length ){

		if ($ ( window ).width() >= 700 ){
			Reference = $('#carrousel li:first-child');

			Refwidth = Reference.width() * 3;

			RefElement = Reference.width() * 2;

			NbElement = ('.carrousel li').length;


			$('.carrousel-container')

				.css( 'width', Refwidth )

				.css( 'height', Reference.height() )

			.after( ''
					+	'<ul class="carrousel-pagination">'
					+	'	<li class="carrousel-prev carrousel-btn"><span>Précédent</span><button class="btn-carrousel btn-prev" type="button"></button></li>'
					+	'	<li class="carrousel-next carrousel-btn"><span>Suivant</span><button class="btn-carrousel btn-next" type="button"></button></li>'
					+	'</ul>'
					+	'');

					Cpt = 0;

				$('.carrousel-next button').click(function() {

					if ( Cpt < (NbElement-1) ){
						Cpt++;
						$('.carrousel').animate({
							marginLeft : - ( RefElement * Cpt )
						});
					}

				});

				//précédent
				$('.carrousel-prev button').click(function() {

					if ( Cpt > 0 ){
						Cpt--;
						$('.carrousel').animate({
							marginLeft : - ( RefElement * Cpt )
						});
					}

				});

				function slideImg(){

					setTimeout( function(){

						if (Cpt < (NbElement-1) ){
							Cpt++;
						}
						else{
							Cpt = 0;
						}
						$('.carrousel').animate({
								marginLeft : - ( RefElement * Cpt )
						});

						slideImg();

					}, 5000 );

				}

				slideImg();
		}else{
			Reference = $('#carrousel li:first-child');

			Refwidth = Reference.width();

			RefElement = Reference.width();

			NbElement = ('.carrousel li').length;


			$('.carrousel-container')

				.css( 'width', Refwidth )

				.css( 'height', Reference.height() )

			.after( ''
					+	'<ul class="carrousel-pagination">'
					+	'	<li class="carrousel-prev carrousel-btn"><span>Précédent</span><button class="btn-carrousel btn-prev" type="button"></button></li>'
					+	'	<li class="carrousel-next carrousel-btn"><span>Suivant</span><button class="btn-carrousel btn-next" type="button"></button></li>'
					+	'</ul>'
					+	'');

					Cpt = 0;

				$('.carrousel-next button').click(function() {

					if ( Cpt < (NbElement-1) ){
						Cpt++;
						$('.carrousel').animate({
							marginLeft : - ( RefElement * Cpt )
						});
					}

				});

				//précédent
				$('.carrousel-prev button').click(function() {

					if ( Cpt > 0 ){
						Cpt--;
						$('.carrousel').animate({
							marginLeft : - ( RefElement * Cpt )
						});
					}

				});

				function slideImg(){

					setTimeout( function(){

						if (Cpt < (NbElement-1) ){
							Cpt++;
						}
						else{
							Cpt = 0;
						}
						$('.carrousel').animate({
								marginLeft : - ( RefElement * Cpt )
						});

						slideImg();

					}, 5000 );

				}

				slideImg();
		}
	}
});
//LAST-SOUND
$(function(){

	if ($("#last-sound").length ){
		var audioPlayer = document.getElementById("audioplayer"),
			audioTrack = document.getElementById("audiotrack"),
			playButton = document.createElement("button");
			playButton.type = "button";
			audioPlayer.appendChild(playButton);
			audioTrack.removeAttribute("controls");

	   function setText(el,text) {
			el.innerHTML = text;
		}

		setText(playButton,"<span class='control control-play'><i>Play</i></span>");
		playButton.addEventListener("click", player);
			function player() {
				if (audioTrack.paused) {
					setText(playButton,"<span class='control control-pause'><i>Pause</i></span>");
					audioTrack.play();
				} else {
					setText(playButton,"<span class='control control-play'><i>Play</i></span>");
					audioTrack.pause();
			}
		}
	}//if $#last-sound
});
//PAGINATION SOUNDS
$(function(){

	if ($ ( window ).width() >= 700 ){

		$('#audio-players').pajinate({
			items_per_page : 4,
			item_container_id : '#list-sounds',
			nav_panel_id : '.nav-sounds',
			nav_label_prev : '<',
			nav_label_next : '>',
			show_first_last : false

		});

	}else{

		$('#audio-players').pajinate({
			items_per_page : 1,
			item_container_id : '#list-sounds',
			nav_panel_id : '.nav-sounds',
			nav_label_prev : '<',
			nav_label_next : '>',
			show_first_last : false

		});

	}

});
//SCROLL
$(function(){
		if( $('.sticky-menu').length ) {
			$('.google-map a[href^="#"]').click(function(f) {

				f.preventDefault();

		        cible=$(this).attr('href');

		        if($(cible).length>=1){
			        haut=$(cible).offset().top;
		        }
		        $('html,body').animate({scrollTop:haut},2000,'easeOutQuint');
		        return false;
		    });
		}
});