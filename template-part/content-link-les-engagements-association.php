<?php
$posts = get_field('les_engagements_association');

if( $posts ): ?>
    <ul class="row d-flex align-items-end">
        <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
            <?php
            setup_postdata($post);
            $bool = get_field('plus_lien');
            $file = get_field('site_web_du_partenaire');
            $files = get_field('sites_web_du_partenaire');

            $size = '4col';
            $contenu = get_the_content();
            $title = get_the_title();

            if ($bool) {
            ?>
                <li class="link-site col-12 col-lg-4">
                    <figure><?= the_post_thumbnail($size) ?></figure>
                    <?= $contenu ?>
                    <a class="d-flex align-items-center col-12 col-lg-10" href="<?= $file ?>" title="Voir le site : <?= $title ?>" target="_blank"><i class="world ti-world col-3"></i><span class="col-9"><?= $title ?></span></a>
                </li>
            <?php
            } else {
            ?>
                <li class="link-sites col-12">
                    <div class="row">
                        <div class="col-12">
                            <figure><?= the_post_thumbnail($size) ?></figure>
                            <?= $contenu ?>
                        </div>
                        <div class="d-flex row col-12">
                            <?php foreach( $files as $file):
                                $patterns = array();
                                $patterns[0] = 'http://';
                                $patterns[1] = 'https://';
                                $patterns[2] = 'www.';
                                $patterns[3] = '/';
                                $file_link = str_replace($patterns, '', $file['url_site']);
                                ?>
                                <a class="d-flex align-items-center col-12 col-lg-5" href="<?= $file['url_site'] ?>" title="Voir le site : <?= $title ?>" target="_blank"><i class="world ti-world col-2"></i><span class="col-9"><?= $file_link ?></span></a>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </li>
            <?php
            }
        endforeach; ?>
        <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
    </ul>
<?php endif; ?>
