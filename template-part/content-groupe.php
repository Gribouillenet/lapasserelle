<?php
    $title_equipe = get_sub_field('title_equipe');
    $equipe = get_sub_field('equipe');
    $title_contact = get_sub_field('title_contact');
    $tel = get_sub_field('tel');
    $text_tel = get_sub_field('text_tel');
    $tel_2 = get_sub_field('tel_2');
    $text_tel_2 = get_sub_field('text_tel_2');
    $adresse = get_sub_field('adresse');
    $contact = get_sub_field('ind_mail');
    $contact_text = get_sub_field('text_url_email');

    $i = 0;
    while( have_rows('info') ): the_row();
        $i += 1;
    endwhile;

    if ( $i == 1 ) {?>
       	<div class="row">
           <?php
            while( have_rows('info') ): the_row();
            ?>
                <div class="bloc_info col-12 col-lg-6">
                    <?php the_sub_field('paragraphe'); ?>
                </div>
            <?php endwhile; ?>

            <div class="col-12 col-lg-6">

				<?php if( $title_equipe ): ?>

	                <div class="bloc_equipe">

	                    <h3><?= $title_equipe ?></h3>
	                    <?= $equipe ?>

	                </div>

	            <?php endif;

	            if($title_contact):?>

		            <div class="bloc_contact">
		                <h3><?= $title_contact ?></h3>
		               <?php if( $tel ):?>
			                <div class="d-flex align-items-center tel">
			                    <i class="fas fa-mobile-alt"></i>
			                    <?php if( $text_tel ):?>
				                    <span><?= $text_tel ?><a class="tel_text" href="tel:<?= $tel ?>" target="_blank" title="Besoin d'un renseignement n'hésitez pas à nous appeler"><?= $tel ?></a></span>
			                    <?php else :?>
			                    	<a href="tel:<?= $tel ?>" target="_blank" title="Besoin d'un renseignement n'hésitez pas à nous appeler"><?= $tel ?></a>
			                    <?php endif;?>
			                </div>
						<?php endif;
						if( $tel_2 ):?>
			                <div class="d-flex align-items-center tel">
				                <i class="fas fa-mobile-alt"></i>
				                <?php if( $text_tel_2 ):?>
			                    <span><?= $text_tel_2 ?><a class="tel_text" href="tel:<?= $tel_2 ?>" target="_blank" title="Besoin d'un renseignement n'hésitez pas à nous appeler"><?= $tel_2 ?></a></span>
			                    <?php else :?>
			                    <a href="tel:<?= $tel_2 ?>" target="_blank" title="Besoin d'un renseignement n'hésitez pas à nous appeler"><?= $tel_2 ?></a>
			                    <?php endif;?>
			                </div>
						<?php endif;

						if( $adresse ):
						?>
		                <div class="d-flex align-items-center adresse">
		                    <i class="fas fa-map-marker-alt"></i><span><?= $adresse ?></span>
		                </div>

		                <?php endif;

			            if( $contact ):
		                ?>
		                <a class="d-flex align-items-center contact" href="<?= $contact ?>" title="Contactez-nous"><i class="fab fa-telegram-plane"></i><?php if($contact_text):?><span><?= $contact_text ?></span><?php endif;?></a>
		                <?php endif;?>
		            </div>

				<?php endif;?>

		</div>
       	</div>
    <?php
    } else {
    ?>

        <div class="row">
            <?php
            while( have_rows('info') ): the_row();
            ?>
                <div class="bloc_info col-12 col-lg-6">
                    <?php the_sub_field('paragraphe'); ?>
                </div>
            <?php endwhile; ?>
        </div>

        <div class="row">

	        <?php if( $title_equipe ): ?>

	            <div class="bloc_equipe col-12 col-lg-6">

	                <h3><?= $title_equipe ?></h3>
	                <?= $equipe ?>

	            </div>

            <?php endif;

            if($title_contact):?>

            	<div class="bloc_contact col-12 col-lg-6">

	                <h3><?= $title_contact ?></h3>
	                <?php if( $tel ):?>
	                <div class="d-flex align-items-center tel">

	                    <i class="fas fa-mobile-alt"></i>
	                    <?php if( $text_tel ):?>
	                    <span><?= $text_tel ?><a class="tel_text" href="tel:<?= $tel ?>" target="_blank" title="Besoin d'un renseignement n'hésitez pas à nous appeler"><?= $tel ?></a></span>

	                    <?php else :?>

	                    <a href="tel:<?= $tel ?>" target="_blank" title="Besoin d'un renseignement n'hésitez pas à nous appeler"><?= $tel ?></a>

						<?php endif;?>

	                </div>
					<?php endif;

					if( $tel_2 ):?>

		                <div class="d-flex align-items-center tel">
			                <i class="fas fa-mobile-alt"></i>

			                <?php if( $text_tel_2 ):?>

		                    <span><?= $text_tel_2 ?><a class="tel_text" href="tel:<?= $tel_2 ?>" target="_blank" title="Besoin d'un renseignement n'hésitez pas à nous appeler"><?= $tel_2 ?></a></span>

		                    <?php else :?>

		                    <a href="tel:<?= $tel_2 ?>" target="_blank" title="Besoin d'un renseignement n'hésitez pas à nous appeler"><?= $tel_2 ?></a>

		                    <?php endif;?>
		                </div>

					<?php endif;

						if( $adresse ):
					?>

		                <div class="d-flex align-items-center adresse">
		                    <i class="fas fa-map-marker-alt"></i><span><?= $adresse ?></span>
		                </div>

	                <?php endif;

		                if( $contact ):
	                ?>
	                	<a class="d-flex align-items-center contact" href="<?= $contact ?>" title="Contactez-nous"><i class="fab fa-telegram-plane"></i><?php if($contact_text):?><span><?= $contact_text ?></span><?php endif;?></span></a>

	                <?php endif;?>

				</div>

			<?php endif;?>

        </div>

    <?php
    }
?>