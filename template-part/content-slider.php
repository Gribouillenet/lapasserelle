<ul class="slider-container">
    <?php
    if( have_rows('testimonials') ):

        while ( have_rows('testimonials') ) : the_row();

            $id = get_the_ID();
            $size = '2col';
            $image = get_sub_field( 'img' );
            $texte = get_sub_field('text_principal');
            $auteur = get_sub_field('auteur');
            $fonction = get_sub_field('fonction');

            ?>

            <li class="item-slide item-slide-<?= $id ?>">
                <article class="d-flex flex-column align-items-center justify-content-center">
                    <?php if($image): ?>
                        <figure><?= wp_get_attachment_image( $image, $size )?></figure>
                    <?php endif; ?>
                    <p><?= $texte ?></p>
                    <aside class="author"><?= $auteur ?></aside>
                    <span><?= $fonction ?></span>
                </article>
            </li>

            <?php
        endwhile;
    endif;
    ?>
</ul>
<div class="slick-navigation"></div>
